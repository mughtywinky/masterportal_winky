/* eslint-disable no-unused-vars */

const Config = {
    ignoredKeys: ["OBJECTID","ID_ADR","ID_ANS","RECHTSWERT","HOCHWERT","CLASSIFY.CLASSVALUE","SHAPE","OBJECTID_12","ROT","AVG_GRID_C","POINT_X","POINT_Y","RASTER.OBJECTID","RASTER.COUNT","BOUNDEDBY","SHAPE_LENGTH","SHAPE_AREA", "GLOBALID", "GEOMETRY", "SHP", "SHP_AREA", "SHP_LENGTH", "GEOM", "THE_GEOM", "OBJECTID_1"],
	wfsImgPath: "https://geoportal.frankfurt.de/img/",
    namedProjections: [
        // GK DHDN
        ["EPSG:31467", "+title=Bessel/Gauß-Krüger 3 +proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs"],
        // ETRS89 UTM
        ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"],
        ["EPSG:8395", "+title=ETRS89/Gauß-Krüger 3 +proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=GRS80 +datum=GRS80 +units=m +no_defs"],
        // WGS84
        ["EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"],
		//Google
		["GOOGLE"]
    ],
	alerting: {
        fetchBroadcastUrl: "https://geoportal.frankfurt.de/v2.8.0/mastercode/broadcastedPortalAlerts.json"
    },
	tree: {
		mapBackground: false
	},
	quickHelp: {imgPath: "./resources/img/"},
    layerConf: "./resources/services-internet.json",
    restConf: "./resources/rest-services-internet.json",
    styleConf: "./resources/style_v3.json",
    scaleLine: true,
    mouseHover: {
        numFeaturesToShow: 2,
        infoText: "(weitere Objekte. Bitte zoomen.)"
    },
    useVectorStyleBeta: true,
	isMenubarVisible: true,
	cesiumParameter: 
	{
        tileCacheSize: 20, //Größe des Tilecaches für Terrain/Raster Kacheln.
        enableLighting: false, //aktiviert Lichteffekte auf dem Terrain von der Sonne aus
        fxaa: false, //aktiviert Fast Approximate Anti-alisasing.
		// Warum Qualität 2?
		maximumScreenSpaceError: 2, //Gibt an wie detailliert die Terrain/Raster Kacheln geladen werden. 4/3 ist die beste Qualität.
		fog: {
            enabled: true,
            density: 0.0002,
            screenSpaceErrorFactor: 2.0
        }
    },
	obliqueMap: true,
	portalLanguage: {
        enabled: true,
        debug: false,
        languages: {
            de: "deutsch",
            en: "englisch"
        },
        fallbackLanguage: "de",
        changeLanguageOnStartWhen: ["querystring", "localStorage", "navigator", "htmlTag"],
    },
    footer: {
        urls: [
            {
                "bezeichnung": "",
                "url": "https://geoportal.frankfurt.de/info/nutzungsbedingungen.html",
                "alias": "Nutzungsbedingungen",
                "alias_mobil": "Copyright"
            },
			{
                "bezeichnung": "",
                "url": "https://geoportal.frankfurt.de/info/datenschutz.html",
                "alias": "Datenschutzerklärung",
                "alias_mobil": "Datenschutz"
            },
			{
                "bezeichnung": "",
                "url": "https://geoportal.frankfurt.de/info/impressum.html",
                "alias": "Impressum",
                "alias_mobil": "Impressum"
            },
			{
                "bezeichnung": "",
                "url": "https://geoportal.frankfurt.de/info/feed.html",
                "alias": "RSS-Feed",
                "alias_mobil": "RSS"
            }
        ],
        showVersion: true
    }
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
    module.exports = Config;
}
